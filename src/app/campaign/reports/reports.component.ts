import { Component, OnInit, ViewChild } from '@angular/core';
import { StatisticsService } from '../../shared/data/statistics.service';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
	reportCount;
	reportData = [];
	rows = [];
	columns = [
        { prop: 'Status' },
        { prop: 'NewsletterName' },
        { prop: 'NewsletterSent' },
        { prop: 'OpenRate' },
        { prop: 'BounceRate' },
        { prop: 'ClickThroughrate' },
        { prop: 'LastSent' },
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(public statisticsservice: StatisticsService, private router: Router, private route: ActivatedRoute) { }

  	ngOnInit() {
  		this.statisticsservice.getreports().then(data => {
	        console.log(data['reports']);
	        if(data['status']){
		        this.reportCount = data['reports'].length;
		        /*for (let i = 0; i < data['reports'].length; i++) {
		        	let array = {
		        		Status : data['reports'][i]['status'],
						NewsletterName : data['reports'][i]['name'],
						NewsletterSent : data['reports'][i]['sent'],
						OpenRate : data['reports'][i]['openRate'],
						BounceRate : data['reports'][i]['bounceRate'],
						ClickThroughrate : data['reports'][i]['linkRate'],
						LastSent : data['reports'][i]['date']
		        	}
		        	this.reportData.push(array);
		        	//this.reportData = data['reports'];
		        }*/
				this.reportData = data['reports'];
		        this.rows = this.reportData;
		    }
	    },
	    error => {
	    });
  }
  
  redirect(id){
	  this.router.navigate(['/statistics'],{queryParams:{id:id}});
  }

}
