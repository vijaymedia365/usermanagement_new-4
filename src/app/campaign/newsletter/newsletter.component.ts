import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { newsLetterService } from '../../shared/data/newsletter.service';
import { ListsService } from '../../shared/data/lists.service';
import { EmailTemplateService } from '../../shared/data/email-template.service';


@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {

  columns = [
    { name: 'Name' },
    { name: 'Subject' },
    { name: 'Status' },
    { name: 'Date' },
    { name: 'Action' }
  ];

	emailTemplateForm: FormGroup;
	newsletterDetails = [];
	condition:boolean = false;
	showTable: boolean = true;
	newsLetter= {
		'name': '',
		'subject': '',
		'list':'Select List',
		'to': '',
		'cc': '',
		'bcc': '',
		'body': '',
    'template': 'Choose Template'
	};
	collectionSize: number;
	p: number = 1;
	pageSize: number = 5;

	public options: Object = {
	    charCounterCount: true,
	    // Set the image upload parameter.
	    imageUploadParam: 'image_param',

	    // Set the image upload URL.
	    imageUploadURL: 'https://click365.com.au/usermanagement/images',

	    // Additional upload params.
	    imageUploadParams: {id: 'my_editor'},

	    // Set request type.
	    imageUploadMethod: 'POST',

	    // Set max image size to 5MB.
	    imageMaxSize: 5 * 1024 * 1024,

	    // Allow to upload PNG and JPG.
	    imageAllowedTypes: ['jpeg', 'jpg', 'png'],
    	events:  {
			'froalaEditor.initialized':  function () {
				console.log('initialized');
			},
  			'froalaEditor.image.beforeUpload':  function  (e,  editor,  images) {
			    //Your code
			    if  (images.length) {
					// Create a File Reader.
					const  reader  =  new  FileReader();
					// Set the reader to insert images when they are loaded.
					reader.onload  =  (ev)  =>  {
					const  result  =  ev.target['result'];
					editor.image.insert(result,  null,  null,  editor.image.get());
					console.log(ev,  editor.image,  ev.target['result'])
					};
					// Read image as base64.
					reader.readAsDataURL(images[0]);
		    	}
		    	// Stop default upload chain.
		    	return  false;
		  	}
		}
	};
	public listDetails: any;
	defaultTemplate;
	emailTemplates;

  	constructor(private formBuilder: FormBuilder, public newsletterservice: newsLetterService, public listsservice: ListsService, public emailtemplateservice: EmailTemplateService) {
  		this.newsLetter.body = this.emailtemplateservice.getDefaultTemplate();
      this.defaultTemplate = this.emailtemplateservice.getDefaultTemplate();
  	}

  	ngOnInit() {
  		this.newsletterservice.getNewletter().then(data => {
  			if(data['status']){
          console.log(data['newsletter']);
  				this.newsletterDetails = data['newsletter'];
  				this.collectionSize = data['newsletter'].length;
  			}
		  });
		  this.listsservice.getlist().then(data => {
	        console.log(data['lists']);
	        this.listDetails = data['lists'];
	    },
	    error => {
	    });
	    this.emailtemplateservice.getNewsTemp(new Date().getTime()).then(data => {
	        console.log(data['emailTemplate']);
	        if(data['status'] == 'success'){
            this.emailTemplates = data['emailTemplate'];
	          //console.log(this.emailTemplates);
	        }
	    },
	    error => {
	    });
  	}
    saveNewsletter(){
  		this.newsletterservice.saveNewletter(this.newsLetter).then(data => {
  			if(data['status']){
          this.showTable = true;
          this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
      			if(data['status']){
      				this.newsletterDetails = data['newsletter'];
      				this.collectionSize = data['newsletter'].length;
              this.newsLetter.body = this.defaultTemplate;
      			}
    		  });
  				//window.location.reload();
  			}
  		});
  	}
    sendnewsletter(id){
  		this.newsletterservice.sendNewletter(id).then(data => {
  			if(data['status']){
          this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
      			if(data['status']){
              this.showTable = true;
      				this.newsletterDetails = data['newsletter'];
      				this.collectionSize = data['newsletter'].length;
              this.newsLetter.body = this.defaultTemplate;
      			}
    		  });
  				//window.location.reload();
  			}
  		});
  	}
    checkvalue(val){
  		console.log(val);
  		if(val != "Select List"){
  			this.condition = true;
  		}
  		else{
  			this.condition = false;
  		}
  	}
  	addnews(){
  		this.showTable = false;
  	}
  	sendtestmail(){
  		console.log(this.newsLetter);
      this.newsletterservice.sendTestEmail(this.newsLetter).then(data => {
  			if(data['status']){
  				this.newsletterDetails = data['newsletter'];
  				this.collectionSize = data['newsletter'].length;
          this.newsLetter.body = this.defaultTemplate;
  			}
		  });
  	}
  	cancel(){
      this.showTable = true;
      this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
  			if(data['status']){
  				this.newsletterDetails = data['newsletter'];
  				this.collectionSize = data['newsletter'].length;
          this.newsLetter.body = this.defaultTemplate;
  			}
		  });
  	}
    displayTemplate(value){
      for(let i = 0; i < this.emailTemplates.length; i++){
        if(this.emailTemplates[i]['id'] == value){
          this.emailtemplateservice.getEmailTempById(value).then(data => {
    	        //console.log(data['emailTemplate']);
    	        if(data['status'] == 'success'){
                this.newsLetter.body = data['emailTemplate'][0]['body'];
    	          //console.log(this.emailTemplates);
    	        }
    	    },
    	    error => {
    	    });
        }
        else{
          this.newsLetter.body =  this.defaultTemplate;
        }
      }
    }
}
