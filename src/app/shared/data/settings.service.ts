import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class SettingsService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    saveSettings(settingDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveSettings.php?q='+this.timestamp, { settingDetail })
            .subscribe(settingValue => {
                console.log(settingValue);               
                resolve(settingValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getSettings() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getSettings.php?q='+this.timestamp)
            .subscribe(settingValue => {
                console.log(settingValue);               
                resolve(settingValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    updateSettings(settingDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/updateSettings.php?q='+this.timestamp, { settingDetail })
            .subscribe(settingValue => {
                console.log(settingValue);               
                resolve(settingValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    /*getEmailTempById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getEmailTemplateById.php?q='+this.timestamp, { id })
            .subscribe(emailtem => {
                console.log(emailtem);               
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteEmailTempById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/deleteEmailTemplateById.php?q='+this.timestamp, { id })
            .subscribe(emailtem => {
                console.log(emailtem);               
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }*/
}