import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import * as XLSX from 'xlsx';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import 'jspdf-autotable';

import { loyaltyService } from '../../shared/data/loyalty.service';
import { loyaltypointService } from '../../shared/data/loyaltypoint.service';

@Component({
  selector: 'app-loyalty-display',
  templateUrl: './loyalty-display.component.html',
  styleUrls: ['./loyalty-display.component.scss']
})
export class LoyaltyDisplayComponent implements OnInit {
	isAdmin: boolean = true;
	user;
	userName;
	loyaltyPoints;
	points = [];
	expirydateArray = [];
	showTable: boolean = true;
	loyaltyDisplay;
	rows = [];
    temp = [];
    exportData = [];

    // Table Column Titles
    columns = [
        { prop: 'Name' },
        { prop: 'PointsRedeemed' },
        { prop: 'UserName' },
        { prop: 'RemainingPoints' },
        { prop: 'RedeemedOn' },
        { prop: 'ExpiryDate' },
    ];
    public loyaltypointdata: any;
    expirydate;
    loyaltypointform = {
      'loyalty': '',
      'pointsredeemed': ''    
    };
    @ViewChild(DatatableComponent) table: DatatableComponent;

	constructor(private LoyaltyService: loyaltyService, private formBuilder: FormBuilder,private route: ActivatedRoute, public router: Router, public LoyaltypointService: loyaltypointService) {
		this.user = JSON.parse(localStorage.getItem('currentUser'));
		if(this.user['role_id'] != "1" && this.user['role_id'] != "2"){
			this.isAdmin = false;
		}
		this.LoyaltyService.getLoyalty().then(data =>{
			console.log(data);
			this.loyaltyPoints = data['loyalty'];
			for (let i = 0; i < data['loyalty'].length; i++) {
			  this.points[data['loyalty'][i]['id']] = data['loyalty'][i]['points'];
			  this.expirydateArray[data['loyalty'][i]['id']] = data['loyalty'][i]['expiryDate'];
			}
		})
	}

	ngOnInit() {
		this.loyaltyDisplay = [];
		if(!this.isAdmin){
			this.userName = this.user['firstName']+' '+this.user['lastName'];
			this.LoyaltypointService.getloyaltypoints(this.user['admin_listid']).then(data => {
				console.log(data['loyalPoints']);
				for (let i = 0; i < data['loyalPoints'].length; i++) {
					let array = {
						Name: data['loyalPoints'][i]['loyalty'],
						PointsRedeemed: data['loyalPoints'][i]['points_redeemed'],
						UserName: data['loyalPoints'][i]['user_id'],
						RemainingPoints: data['loyalPoints'][i]['remaining_points'],
						RedeemedOn: data['loyalPoints'][i]['redeemed_on'],
						ExpiryDate: data['loyalPoints'][i]['expiry_date'],
					}
					this.loyaltyDisplay.push(array);
					this.exportData.push(data['loyalPoints'][i]);
				}
				this.rows = this.loyaltyDisplay;
			},
			error => {
			});
		}
		else{
			this.LoyaltypointService.getallloyaltypoints().then(data => {
				this.loyaltyDisplay = [];
				for (let i = 0; i < data['loyalPoints'].length; i++) {
					let array = {
						Name: data['loyalPoints'][i]['loyalty'],
						PointsRedeemed: data['loyalPoints'][i]['points_redeemed'],
						UserName: data['loyalPoints'][i]['user_id'],
						RemainingPoints: data['loyalPoints'][i]['remaining_points'],
						RedeemedOn: data['loyalPoints'][i]['redeemed_on'],
						ExpiryDate: data['loyalPoints'][i]['expiry_date'],
					}
					this.loyaltyDisplay.push(array);
					this.exportData.push(data['loyalPoints'][i]);
				}
				console.log(this.loyaltyDisplay);
				this.rows = this.loyaltyDisplay;
			},
			error => {
			});
		}
	}

	updateFilter(filterValue: string) {
		this.LoyaltypointService.getallloyaltypoints().then(data => {
			this.loyaltyDisplay = data['loyalPoints'];
			filterValue = filterValue.trim(); // Remove whitespace
			filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
			this.loyaltyDisplay.filter = filterValue;
		});
	}

	redeemPoints(){
      this.loyaltypointdata = {
        loyalty: this.loyaltypointform.loyalty,
        userid: this.user['id'],
        pointsredeemed: this.loyaltypointform.pointsredeemed,
        remainingpoints: (parseInt(this.user['points']) - parseInt(this.loyaltypointform.pointsredeemed)),
        expirydate: this.expirydate
      }
      this.LoyaltypointService.addloyaltypoint(this.loyaltypointdata).then(data => {
        console.log(data);
        if(data['status'] == 'success'){
          window.location.reload();
        }
      },
      error => {
      });
    }

	toggleDisplay() {
      this.showTable = false;
    }

    setPoints(value){
      this.loyaltypointform.pointsredeemed = this.points[value];
      this.expirydate = this.expirydateArray[value];
    }

	ExportToPdf(){
		console.log(this.exportData);
		var doc = new jspdf();
		var col = ['Name', 'Points', 'User Name', 'Total Points', 'Redeemed On', 'Expire Date'];
		var rows = [];
		this.exportData.forEach(element => {      
			var temp = [element.loyalty, element.points_redeemed, element.user_id, element.remaining_points, element.redeemed_on, element.expiry_date];
			rows.push(temp);
		}); 
		doc.autoTable(col, rows, { startY: 10 });    
		doc.save('renewal_'+ new Date().getTime() +'.pdf');
	}

	ExportTOExcel(){
		const workBook = XLSX.utils.book_new();
		const workSheet = XLSX.utils.json_to_sheet(this.exportData);

		XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
		XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.xlsx');
	}

	ExportTOCsv(){
		const workBook = XLSX.utils.book_new();
		const workSheet = XLSX.utils.json_to_sheet(this.exportData);
		const csv = XLSX.utils.sheet_to_csv(workSheet);

		XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
		XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.csv');
	}
}