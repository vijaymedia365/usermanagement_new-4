import { Component, OnInit, ViewChild } from '@angular/core';
import { RenewalService } from '../shared/data/renewal.service';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

@Component({
  selector: 'app-renewals',
  templateUrl: './renewals.component.html',
  styleUrls: ['./renewals.component.scss']
})
export class RenewalsComponent implements OnInit {
	renewals;
	pendingRenewals;
	recentRenewal: boolean = false;
	pendingRenewal: boolean = false;
	sentEmail = false;
	columns = [
        { name: 'Transaction ID' },
        { name: 'User ID' },
        { name: 'Membership ID' },
        { name: 'Amount' },
        { name: 'Renewal Date' },
        { name: 'Expiry Date' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

	constructor(public renewalservice: RenewalService) {
		this.renewals = [];
		this.pendingRenewals = [];
		this.recentRenewal = false;
		this.pendingRenewal = false;
	}

	ngOnInit() {
		this.recentRenewal = true;
		this.pendingRenewal = false;
		this.renewalservice.getRenewal().then(data => {
			if(data['status'] == 'success'){
	    		for (let i = 0; i < data['renewal'].length; i++) {
	    			if(data['renewal'][i]['renewed'] == '0'){
	    				this.renewals.push(data['renewal'][i]);
						console.log(this.renewals);
	    			}
	    			else{
	    				this.pendingRenewals.push(data['renewal'][i]);
						console.log(this.pendingRenewals);
	    			}	    			
	    		}
			}
		});
	}

	recRenewal() {
		this.recentRenewal = true;
		this.pendingRenewal = false;
	}

	pendRenewal() {
		this.pendingRenewal = true;
		this.recentRenewal = false;
	}

	sendEmail(id, email, name, plans, expire_date){
    	this.renewalservice.sendRenewalEmail(id, email, name, plans, expire_date).then(data => {
    		if(data['status'] == 'success'){
    			this.sentEmail = true;
    		}
    	});
    }

}
