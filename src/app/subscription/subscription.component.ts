import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { SubscriptionService } from '../shared/data/subscription.service';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
	personalForm: FormGroup;

	constructor(public subscriptionService: SubscriptionService) {
		this.subscriptionService.getSubscription().then(data => {
			console.log('Get Subscription list - ' + JSON.stringify(data));
		});
	}

	ngOnInit() {
		this.personalForm = new FormGroup({
			'type': new FormControl(null, [Validators.required]),
			'campaign': new FormControl(null, [Validators.required]),
			'members': new FormControl(null, [Validators.required])
	    }, {updateOn: 'blur'});
	}

	saveSubscription(){
		let subscriptionDetail = {
			'type': this.personalForm.controls['type'].value,
			'campaign': this.personalForm.controls['campaign'].value,
			'members': this.personalForm.controls['members'].value
		}
		this.subscriptionService.saveSubscription(subscriptionDetail).then(data => {
			console.log('Data of saved sub - ' + JSON.stringify(data));
			if(data['status']){
				//window.location.reload();
			}
		});
	}
}
