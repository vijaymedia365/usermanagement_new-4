import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { AuthenticationService } from '../../../shared/data/authentication.service';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {

    //@ViewChild('f') loginForm: NgForm;
    loginForm: FormGroup;
    style = 'material';
    title = 'Snotify title!';
    body = 'Lorem ipsum dolor sit amet!';
    timeout = 3000;
    position: SnotifyPosition = SnotifyPosition.centerTop;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;
    
    constructor(private snotifyService: SnotifyService, private router: Router,
        private route: ActivatedRoute, public authenticationService: AuthenticationService) { }
    
    ngOnInit(){
        this.loginForm = new FormGroup({
            'inputUser': new FormControl(null, [Validators.required]),
            'inputPass': new FormControl(null, [Validators.required])
        });
    }
    
    getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
    }

    // On submit button click
    get f() { return this.loginForm.controls; }
    onSubmit() {
        if (this.loginForm.invalid) {
            //alert('Enter Username/Password.');
            this.snotifyService.error('Enter Username/Password.', 'Login Failed', this.getConfig());
            return;
        }
        this.authenticationService.login(this.f.inputUser.value, this.f.inputPass.value).then(data => {
            console.log(data);
            if (data && data['status'] == 'success') {
                localStorage.setItem('isLoggedin', 'true');
                this.router.navigate(['/dashboard/charts']);
            }
            else{
                //alert('Invalid Credentials Please try again.');
                this.snotifyService.error('Invalid Credentials', 'Login Failed', this.getConfig());
            }
        },
        error => {
        });
        //this.loginForm.reset();
    }
    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }
}
